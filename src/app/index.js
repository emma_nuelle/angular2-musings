import { Component, View } from 'angular2/core';
import { RouteConfig, ROUTER_DIRECTIVES } from 'angular2/router';

import Hello from './components/hello';
import Ciao from './components/ciao';
import Linker from './components/linker';
import Chosen from './components/chosen';

import Greeter from './services/greeter';


@Component({
  selector: 'main-app',
  viewProviders: [Greeter]
})

@View({
  directives: [ROUTER_DIRECTIVES, Linker, Chosen],
  templateUrl: '/template.html'
})

@RouteConfig([
  { path: '/', component: Hello, as: 'Hello' },
  { path: '/ciao/:name', component: Ciao, as: 'Ciao' }
])

export default class MainApp {
}
