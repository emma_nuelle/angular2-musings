import { Component, Input } from 'angular2/core';


@Component({
  selector: 'linker',
  templateUrl: '/components/linker/template.html'
})
export default class Linker {
  @Input() name;
  @Input() url;

  constructor() {
  }
}
