import { Component } from 'angular2/core';
import { RouteParams } from 'angular2/router';

import Greeter from '../../services/greeter';


@Component({
  selector: 'ciao',
  templateUrl: '/components/ciao/template.html'
})

export default class Ciao {
  constructor(greeter: Greeter, routeParams: RouteParams) {
    this.message = greeter.say('ciao', routeParams.get('name'));
  }
}
