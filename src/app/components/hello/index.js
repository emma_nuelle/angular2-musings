import { Component } from 'angular2/core';

import Greeter from '../../services/greeter';


@Component({
  selector: 'hello',
  templateUrl: '/components/hello/template.html'
})

export default class Hello {
  constructor(greeter: Greeter) {
    this.message = greeter.say('hello Leila', 'Angular 2');
  }
}

