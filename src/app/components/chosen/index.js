import { Component, ElementRef, AfterViewInit } from 'angular2/core';


@Component({
  selector: 'chosen',
  templateUrl: '/components/chosen/template.html',
})

export default class Chosen implements AfterViewInit {

  constructor(el: ElementRef) {
    // super();
    this.el = el;
  }

  static chosenInitialized = false;
 
  items = [
    {
      id: 'a',
      value: 'First letter'
    }, {
      id: 'b',
      value: 'Second letter'
    }, {
      id: 'c',
      value: 'Thid letter'
    }
  ];
  selectedValue = 'b';

  ngAfterViewInit() {
    if (!Chosen.chosenInitialized) {
      window.jQuery(this.el.nativeElement)
        .find('select')
        .chosen()
        .on('change', (e, args) => {
          this.selectedValue = args.selected;
        });
      Chosen.chosenInitialized = true;
    }
  }
}
