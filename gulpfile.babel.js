import gulp from 'gulp';
import gutil, { PluginError } from 'gulp-util';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import sourcemaps from 'gulp-sourcemaps';
import sass from 'gulp-sass';

import assign from 'object-assign';
import browserify from 'browserify';
import watchify from 'watchify';
import babelify from 'babelify';

import del from 'del';

import BrowserSync from 'browser-sync';
import url from 'url';
import fs from 'fs';

import eslint from 'gulp-eslint';

import cachebust from 'gulp-cache-bust';

const browserSync = BrowserSync.create();

gulp.task('sass', () => {
  return gulp.src('./src/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/assets/css'));
});

gulp.task('copy', ['sass'], () => {
  gulp.src([
    'src/assets/**/*',
    'src/vendor/**/*'
  ])
    .pipe(gulp.dest('dist/assets'));
  return gulp.src([
    'src/index.html',
    'src/app/**/*.html',
    'node_modules/zone.js/dist/zone-microtask.js'
  ])
    .pipe(gulp.dest('dist'));
});

gulp.task('build', ['lint', 'copy'], () => {
  const b = browserify('src/index.js', { debug: true })
    .transform(babelify);
  return bundle(b)
});

gulp.task('lint', () => {
  return gulp.src(['src/app/**/*.js', 'src/index.js'])
    .pipe(eslint())
    .pipe(eslint.format())
});

gulp.task('watch', () => {
  const b = browserify('src/index.js', assign({ debug: true }, watchify.args))
    .transform(babelify);
  const w = watchify(b)
    .on('update', () => bundle(w))
    .on('log', gutil.log);
  return bundle(w)
});

gulp.task('sass-watch', () => {
  gulp.watch('./src/**/*.scss', ['sass'])
});

gulp.task('clean', () => {
  return del('dist');
});

gulp.task('reload', ['lint'], () => {
  browserSync.reload()
  return true;
});

gulp.task('serve', ['watch'], () => {
  const defaultFile = 'index.html';
  const folder = './dist';

  browserSync.init({
    server: {
      baseDir: folder,
      middleware: (req, res, next) => {
        let fileName = url.parse(req.url);
        fileName = fileName.href.split(fileName.search).join("");
        const fileExists = fs.existsSync(folder + fileName);
        if (!fileExists && fileName.indexOf("browser-sync-client") < 0) {
            req.url = "/" + defaultFile;
        }
        return next();
      }
    }
  });

  gulp.watch(['./dist/*.js'], ['reload']);
  gulp.watch(['./src/**/*.html', './src/**/*.scss'], ['copy', 'reload']);
});

gulp.task('prod', ['build'], () => {
  gulp.src('./dist/index.html')
    .pipe(cachebust())
    .pipe(gulp.dest('./dist'));
});

gulp.task('default', ['copy', 'build', 'serve']);

function bundle(b) {
  return b.bundle()
    .on('error', (e) => {
      console.error(e.stack);
    })
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('dist'));
}
